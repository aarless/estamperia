#!/usr/bin/env python
# -*- coding: utf-8 -*-

from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
from sys import argv

# Argumentos da linha digitável
entrada = argv[1]
color = argv[2]
img = argv[3]
w = argv[4]
h = argv[5]
m = argv[6]

# Abre arquivo de texto
with open(entrada) as f:
    texto = f.read().decode("utf-8")

# Cria a WordCloud
wordcloud = WordCloud(font_path='path/to/font',
                          stopwords=STOPWORDS,
                          background_color='rgba(255, 255, 255, 0)',
                          mode='RGBA',
                          max_words=int(m),
                          colormap=color,
                          width=int(w),
                          height=int(h),
                         ).generate(texto)

# Salva a WordCloud
plt.imshow(wordcloud)
plt.axis('off')
plt.savefig(img, transparent=True)
